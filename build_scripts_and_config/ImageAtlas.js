let PROJECT_CONFIG = require("./project.config");

let texturePacker = require('gulp-free-tex-packer');
let through = require('through2');
let glob = require('glob');
let imageResize = require('gulp-image-resize');
let path = require('path');
let gulp = require("gulp");

let spriteNameMap = {};

module.exports = {

	buildFromFolder: function (folder, assetManifest) {

		let name = folder;

		if (name == '*') {
			name = 'default';

			folder = PROJECT_CONFIG.PATHS.IMAGE_SRC_PATH + '*.{png,jpg}';
		} else {

			folder = PROJECT_CONFIG.PATHS.IMAGE_SRC_PATH + folder + '/**/**.{png,jpg}';
		}

		// go through (current) folder - any duplicate filenames?
		performAtlasSanityCheckSync(folder);

		// setup assetManifest alias reference
		let newLength = assetManifest.atlases.push({
			alias: name,
			src: []
		});

		let atlasRef = assetManifest.atlases[newLength - 1];

		assetManifest.qualitySettings = PROJECT_CONFIG.IMAGE_QUALITY_SETTINGS;
		assetManifest.designDimensions = PROJECT_CONFIG.ASSET_DESIGN_DIMENSIONS;

		let promises = [];
		PROJECT_CONFIG.IMAGE_QUALITY_SETTINGS.forEach((qualitySetting) => {
			promises.push(generateImageAtlasForQualitySetting(qualitySetting, name, folder, atlasRef));
		});

		return Promise.all(promises);
	}
}

function generateImageAtlasForQualitySetting(qualitySetting, name, folder, atlasRef) {

	let texturePackerOptions = JSON.parse(JSON.stringify(PROJECT_CONFIG.TEXTUREPACKER_OPTIONS));
	texturePackerOptions.textureName = name;
	texturePackerOptions.width = qualitySetting.spriteSheetSize;
	texturePackerOptions.height = qualitySetting.spriteSheetSize;
	texturePackerOptions.scale = qualitySetting.percentage / 100;

	let subFolder = qualitySetting.alias;

	let scale = getScaleForQualitySetting(qualitySetting);

	let promise = new Promise((resolve, reject) => {
		gulp.src(folder)
			.pipe(imageResize({
				imageMagick: true,
				percentage: scale * 100,
				filter: "Catrom" //Good at downsampling
			}))
			.pipe(texturePacker(texturePackerOptions))
			.pipe(through.obj(function (chunk, enc, chunkDone) {
				// chunk -> https://github.com/gulpjs/vinyl
				// push path chunk.json to atlasRef.src (array)
				if (chunk.extname == '.json') {
					let path = (PROJECT_CONFIG.PATHS.RELATIVE_OUTPUT_ATLASES + "{0}\\" + chunk.relative).replace(/\\/g, '/');
					if (!atlasRef.src.includes(path)) {
						atlasRef.src.push(path);
					}
				};
				chunkDone(null, chunk)
			}))
			.pipe(gulp.dest(PROJECT_CONFIG.PATHS.OUTPUT_PATH_ATLASES + subFolder))
			.on('end', resolve);
	})

	return promise;
}

function getScaleForQualitySetting(qualitySetting) {

	let canvasScaleFactor = 1;
	if (PROJECT_CONFIG.ASSET_DESIGN_DIMENSIONS.height > PROJECT_CONFIG.ASSET_DESIGN_DIMENSIONS.width) {
		canvasScaleFactor = qualitySetting.targetCanvasDimensions.height / PROJECT_CONFIG.ASSET_DESIGN_DIMENSIONS.height;
	} else {
		canvasScaleFactor = qualitySetting.targetCanvasDimensions.width / PROJECT_CONFIG.ASSET_DESIGN_DIMENSIONS.width;
	}
	return 1 * canvasScaleFactor * (qualitySetting.percentage / 100);
}

/**
 * Performs all the buildtime checks for the atlas creation:
 *   - checks for duplicate sprite names across atlases
 * 
 * @param {any} config The object defining the atlases
 * @returns {boolean} True if no errors were found
 */

function performAtlasSanityCheckSync(folder) {

	glob(folder, {}, function (er, files) {

		for (var i = 0; i < files.length; i++) {

			var spriteName = path.parse(files[i]).name;

			if (spriteNameMap[spriteName]) {
				console.error('Duplicate sprite names found: <' + spriteName + '> in -> ', folder);
			}

			spriteNameMap[spriteName] = true;
		}
	});

	return true;
}