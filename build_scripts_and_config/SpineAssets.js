
let gulpJsonMinify = require('gulp-jsonminify');
let glob = require('glob');
let PROJECT_CONFIG = require("./project.config");
let path = require('path');
let gulp = require("gulp");

module.exports = {

	build: async function (assetManifest) {
		let spineFiles = [];

		let files = glob.sync(PROJECT_CONFIG.PATHS.SPINE_SRC_PATH + "**.json", {});

		spineFiles = files;

		for (var i = 0; i < files.length; i++) {

			let spineFile = require(PROJECT_CONFIG.PATHS.SPINE_SRC_PATH + path.parse(files[i]).base);

			let requiredImages = [];
			for (const key in spineFile.slots) {
				if (spineFile.slots.hasOwnProperty(key)) {
					requiredImages.push(spineFile.slots[key].name);
				}
			}

			let animationNames = [];
			for (const key in spineFile.animations) {
				if (spineFile.animations.hasOwnProperty(key)) {
					animationNames.push(key);
				}
			}

			let requiredImageAtlases = [];
			requiredImages.forEach(requiredImage => {

				let imageFiles = glob.sync(PROJECT_CONFIG.PATHS.IMAGE_SRC_PATH + '**/**/*.{png,jpg}', {});

				imageFiles.forEach(imageFile => {

					let imageFileName = path.parse(imageFile).name;

					if (requiredImage == imageFileName) {
						let folderPath = path.parse(imageFile).dir + "/";

						let requiredImageAtlasName = folderPath.replace(PROJECT_CONFIG.PATHS.IMAGE_SRC_PATH, '');

						if (requiredImageAtlasName === "") {
							requiredImageAtlasName = "default";
						} else {
							requiredImageAtlasName = requiredImageAtlasName.slice(0, requiredImageAtlasName.length - 1);
						}

						if (!requiredImageAtlases.includes(requiredImageAtlasName)) {
							requiredImageAtlases.push(requiredImageAtlasName);
						}
					}
				});
			});

			let spineFileName = path.parse(files[i]).name;

			assetManifest.spines.push({
				alias: spineFileName,
				src: [PROJECT_CONFIG.PATHS.RELATIVE_OUTPUT_SPINE + spineFileName + ".json"],
				atlas: requiredImages,
				animations: animationNames,
				requiredImageAtlas: requiredImageAtlases
			});
		}

		for (let i = 0; i < spineFiles.length; i++) {
			await copySpineFile(spineFiles[i]);
		}

		return Promise.resolve();
	}
}

function copySpineFile(src) {

	return new Promise((resolve, reject) => {
		gulp.src(src)
			.pipe(gulpJsonMinify())
			.pipe(gulp.dest(PROJECT_CONFIG.PATHS.OUTPUT_PATH_SPINE))
			.on('end', resolve);
	});
}