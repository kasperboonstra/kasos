let PROJECT_CONFIG = require("./project.config");

let assetManifest;

module.exports = {
	generateFromManifest: function (_assetManifest, supportedLanguages) {
		let fileContent = 'import { Cool } from "@coolgames/pixi-framework";\n';
		fileContent += "// tslint:disable: max-classes-per-file\n";

		assetManifest = _assetManifest;
		for (const key in assetManifest) {
			if (assetManifest.hasOwnProperty(key)) {
				const assetManifestEntry = assetManifest[key];

				if (!Array.isArray(assetManifestEntry) || key === "supportedLanguages") {
					continue;
				}

				fileContent += generateAssetMapCodeFromAssetManifestEntry(assetManifestEntry);

				if (assetManifestEntry === assetManifest.atlases) {

					let codeLine = getClassDeclaration(PROJECT_CONFIG.ASSETMAP_CONFIG.IMAGE_ASSET_CLASS_NAME);

					assetManifest.atlases.forEach(atlas => {

						atlas.src.forEach(atlasFile => {
							let imageAtlas = require(StringFormat(PROJECT_CONFIG.PATHS.OUTPUT_PATH + atlasFile, PROJECT_CONFIG.IMAGE_QUALITY_SETTINGS[0].alias));

							for (const key in imageAtlas.frames) {
								if (imageAtlas.frames.hasOwnProperty(key)) {
									codeLine += generateLineForKey(key, PROJECT_CONFIG.ASSETMAP_CONFIG.IMAGE_TYPE);
								}
							}
						});

					});
					codeLine = appendEndline(codeLine);

					fileContent += codeLine;
				}

				if (assetManifestEntry === assetManifest.sounds) {

					let codeLine = getClassDeclaration(PROJECT_CONFIG.ASSETMAP_CONFIG.SOUND_ASSET_CLASS_NAME);

					assetManifest.sounds.forEach(atlas => {
						atlas.src.forEach(atlasFile => {
							let soundSpriteAtlas = require(PROJECT_CONFIG.PATHS.OUTPUT_PATH + atlasFile);

							for (const key in soundSpriteAtlas.spritemap) {
								if (soundSpriteAtlas.spritemap.hasOwnProperty(key)) {
									codeLine += generateLineForKey(key, PROJECT_CONFIG.ASSETMAP_CONFIG.SOUND_TYPE);
								}
							}
						});

					});
					codeLine = appendEndline(codeLine);

					fileContent += codeLine;
				}
			}
		}

		let localizationFileContent = require(PROJECT_CONFIG.PATHS.OUTPUT_PATH_LOCALIZATION + PROJECT_CONFIG.PATHS.OUTPUT_LOCALIZATION_FILENAME);

		fileContent += getLocalizationCode(localizationFileContent, supportedLanguages);

		return fileContent;
	}
}

function generateAssetMapCodeFromAssetManifestEntry(assetManifestEntry) {

	if (assetManifestEntry.length <= 0) {
		return "";
	}

	let className = getClassName(assetManifestEntry);
	let type = getType(assetManifestEntry);

	if (type === PROJECT_CONFIG.ASSETMAP_CONFIG.SPINE_TYPE) {
		return generateSpineCode(className, assetManifestEntry);
	}

	let classCode = getClassDeclaration(className);

	assetManifestEntry.forEach(atlas => {
		classCode += generateLineForKey(atlas.alias, type);
	});

	classCode = appendEndline(classCode);

	return classCode;
}

function getClassDeclaration(className, optionalInheritence) {
	let inheritence = optionalInheritence !== undefined ? " extends " + optionalInheritence : "";

	let code = 'export class ' + className + inheritence + '\n{\n';
	return code;
}

function appendEndline(codeLine) {
	codeLine += '}\n';
	return codeLine;
}

function generateLineForKey(key, type) {
	let value = key;
	//subsitute dashes and whitespaces with underscores
	key = convertToCodeSafe(key);

	return '	public static ' + key.toUpperCase() + ': ' + type + ' = new ' + type + '("' + value + '");\n';
}

function generateSpineCode(className, assetManifestEntry) {

	let spineCode = "";

	assetManifestEntry.forEach(spine => {

		let animationClassName = getSpineAnimationClassName(spine.alias);

		spineCode += getClassDeclaration(animationClassName, PROJECT_CONFIG.ASSETMAP_CONFIG.SPINE_TYPE);

		spine.animations.forEach(animation => {
			spineCode += generateStringDeclarationForKey(animation);
		})
		spineCode = appendEndline(spineCode);
	});

	spineCode += getClassDeclaration(className);

	assetManifestEntry.forEach(spine => {
		let animationType = getSpineAnimationClassName(spine.alias);
		spineCode += generateLineForKey(spine.alias, animationType);
	});

	spineCode = appendEndline(spineCode);

	return spineCode;
}

function getSpineAnimationClassName(spineName) {
	let animationClassName = spineName.charAt(0).toUpperCase() + spineName.slice(1) + "Animation";
	animationClassName = convertToCodeSafe(animationClassName);
	return animationClassName;
}

function generateStringDeclarationForKey(key) {
	let value = key;
	//subsitute dashes and whitespaces with underscores
	key = convertToCodeSafe(key);

	return '	public ' + key.toUpperCase() + ': string = "' + value + '";\n';
}

function getLocalizationCode(localizationFileContent, supportedLanguages) {

	// Add localization keys 
	let localizationCode = "";
	localizationCode += getClassDeclaration(PROJECT_CONFIG.ASSETMAP_CONFIG.LOCALIZATION_CLASS_NAME);

	for (const key in localizationFileContent) {
		localizationCode += generateLineForKey(key, PROJECT_CONFIG.ASSETMAP_CONFIG.LOCALIZATION_TYPE);
	}

	localizationCode = appendEndline(localizationCode);

	// Supported languages
	localizationCode += getClassDeclaration(PROJECT_CONFIG.ASSETMAP_CONFIG.SUPPORTED_LANGUAGES_CLASS_NAME);

	for (let i = 0; i < supportedLanguages.length; i++) {
		const languageKey = supportedLanguages[i];
		localizationCode += generateLineForKey(languageKey, PROJECT_CONFIG.ASSETMAP_CONFIG.LANGUAGE_TYPE);
	}

	localizationCode = appendEndline(localizationCode);

	return localizationCode;
}

function convertToCodeSafe(string) {
	return string.replace(/[\s-]/g, "_");
}

function getClassName(assetManifestEntry) {

	switch (assetManifestEntry) {
		case assetManifest.atlases:
			return PROJECT_CONFIG.ASSETMAP_CONFIG.ATLAS_ASSET_CLASS_NAME;
		case assetManifest.sounds:
			return PROJECT_CONFIG.ASSETMAP_CONFIG.AUDIOSPRITE_ASSET_CLASS_NAME;
		case assetManifest.spines:
			return PROJECT_CONFIG.ASSETMAP_CONFIG.SPINE_ASSET_CLASS_NAME;
		case assetManifest.fonts:
			return PROJECT_CONFIG.ASSETMAP_CONFIG.FONT_ASSET_CLASS_NAME;
		case assetManifest.qualitySettings:
			return PROJECT_CONFIG.ASSETMAP_CONFIG.QUALITY_CLASS_NAME;
		default:
			console.error("CLASS GENERATION FOR " + assetManifestEntry + " NOT IMPLEMENTED YET");
			break;
	}
}

function getType(assetManifestEntry) {

	switch (assetManifestEntry) {
		case assetManifest.atlases:
			return PROJECT_CONFIG.ASSETMAP_CONFIG.ATLAS_TYPE;
		case assetManifest.sounds:
			return PROJECT_CONFIG.ASSETMAP_CONFIG.AUDIOSPRITE_TYPE;
		case assetManifest.spines:
			return PROJECT_CONFIG.ASSETMAP_CONFIG.SPINE_TYPE;
		case assetManifest.fonts:
			return PROJECT_CONFIG.ASSETMAP_CONFIG.FONT_TYPE;
		case assetManifest.qualitySettings:
			return PROJECT_CONFIG.ASSETMAP_CONFIG.QUALITYSETTING_TYPE;
		default:
			console.error("CLASS GENERATION FOR " + assetManifestEntry + " NOT IMPLEMENTED YET");
			break;
	}
}

function StringFormat() {
	// The string containing the format items (e.g. "{0}")
	// will and always has to be the first argument.
	var theString = arguments[0];

	// start with the second argument (i = 1)
	for (var i = 1; i < arguments.length; i++) {
		// "gm" = RegEx options for Global search (more than one instance)
		// and for Multiline search
		var regEx = new RegExp("\\{" + (i - 1) + "\\}", "gm");

		theString = theString.replace(regEx, arguments[i]);
	}

	return theString;
};