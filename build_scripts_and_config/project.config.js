let path = require('path');

module.exports = (function () {

	let configurationObject = {};

	/**
	 * PATHS CONFIGURATION
	 */
	let PATHS = {};
	PATHS.ROOT = path.join(__dirname, "../");
	PATHS.SRC = PATHS.ROOT + "src/"

	PATHS.ASSETS_FOLDER = PATHS.ROOT + "assets/";

	PATHS.ASSETMAP_OUTPUT = PATHS.SRC + "assetmap/"

	PATHS.IMAGE_SRC_PATH = PATHS.ASSETS_FOLDER + "images/";
	PATHS.AUDIO_SRC_PATH = PATHS.ASSETS_FOLDER + "audio/";
	PATHS.FONT_SRC_PATH = PATHS.ASSETS_FOLDER + "fonts/";
	PATHS.SPINE_SRC_PATH = PATHS.ASSETS_FOLDER + "json/";

	PATHS.OUTPUT_PATH = PATHS.ROOT + "build/";

	PATHS.OUTPUT_PATH_ATLASES = PATHS.OUTPUT_PATH + "atlases/";
	PATHS.OUTPUT_PATH_SPINE = PATHS.OUTPUT_PATH + "spine/";
	PATHS.OUTPUT_PATH_AUDIO = PATHS.OUTPUT_PATH + "audio/";
	PATHS.OUTPUT_PATH_FONT = PATHS.OUTPUT_PATH + "fonts/";

	PATHS.OUTPUT_PATH_LOCALIZATION = PATHS.OUTPUT_PATH;

	PATHS.RELATIVE_OUTPUT_ATLASES = "atlases/";
	PATHS.RELATIVE_OUTPUT_SPINE = "spine/";
	PATHS.RELATIVE_OUTPUT_AUDIO = "audio/";
	PATHS.RELATIVE_OUTPUT_FONT = "fonts/";
	PATHS.RELATIVE_OUTPUT_LOCALIZATION = "./";

	PATHS.OUTPUT_PATH_MANIFEST = PATHS.OUTPUT_PATH + "assets-manifest.json";
	PATHS.OUTPUT_LOCALIZATION_FILENAME = "localization.json";

	PATHS.TSCONFIG = PATHS.ROOT + "tsconfig.json";

	PATHS.BUILD_SCRIPTS_AND_CONFIG = PATHS.ROOT + "build_scripts_and_config/"

	PATHS.FBAPP_CONFIG = PATHS.BUILD_SCRIPTS_AND_CONFIG + "fbapp-config.json";

	configurationObject.PATHS = PATHS;

	/**
	 * ASSETMAP CONFIGURATION
	 */
	configurationObject.ASSETMAP_CONFIG = {
		ATLAS_ASSET_CLASS_NAME: "AssetAtlas",
		IMAGE_ASSET_CLASS_NAME: "AssetImage",
		SPINE_ASSET_CLASS_NAME: "AssetSpine",
		AUDIOSPRITE_ASSET_CLASS_NAME: "AssetAudioSprite",
		SOUND_ASSET_CLASS_NAME: "AssetSound",
		FONT_ASSET_CLASS_NAME: "AssetFont",
		QUALITY_CLASS_NAME: "QualitySetting",
		LOCALIZATION_CLASS_NAME: "LocalizedText",
		SUPPORTED_LANGUAGES_CLASS_NAME: "SupportedLanguage",

		ATLAS_TYPE: "Cool.AtlasAsset",
		IMAGE_TYPE: "Cool.ImageAsset",
		SPINE_TYPE: "Cool.SpineAsset",
		AUDIOSPRITE_TYPE: "Cool.AudioSpriteAsset",
		SOUND_TYPE: "Cool.SoundAsset",
		FONT_TYPE: "Cool.FontAsset",
		QUALITYSETTING_TYPE: "Cool.QualitySetting",
		LOCALIZATION_TYPE: "Cool.LocalizationKey",
		LANGUAGE_TYPE: "Cool.Language",

		FILENAME: "AssetMap.ts"
	};

	/**
	 * IMAGE CONFIGURATION
	 */
	configurationObject.ASSET_DESIGN_DIMENSIONS = {
		width: 240,
		height: 320
	};

	configurationObject.IMAGE_QUALITY_SETTINGS = [{
		alias: "standard",
		percentage: 100,
		maxResolution: Number.MAX_SAFE_INTEGER,
		spriteSheetSize: 1024,
		targetCanvasDimensions: {
			width: 240,
			height: 320
		}
	}];

	configurationObject.TEXTUREPACKER_OPTIONS = {

		fixedSize: false,
		padding: 4,
		allowRotation: false,
		detectIdentical: true,
		allowTrim: false,
		exporter: "Pixi",
		removeFileExtension: true,
		prependFolderName: false
		// tinify: false,
		// tinifyKey: ""
	};

	configurationObject.DEPLOY_CONFIG = {
		DEV: {
			appId: "654004691704493",
			access_token: "EAAYsfZAxiFmMBALGZCqEDuOEZCPS9eZC0BBTIZCQxXziAWZAQm5yoxTZAXEybztLVcURiTABHnuCxiMeqqtT0CSxjZBQFBBjAzkk2iTDVZAZCsioPN4yBTf4Xp859XaLgcLF1POxlnbep6lZCldg39QFKPZB2kEcChRh0yGyQIZALEZBAZCfwZDZD",
		},
		STAGE: {
			appId: "654004691704493",
			access_token: "EAAYsfZAxiFmMBALGZCqEDuOEZCPS9eZC0BBTIZCQxXziAWZAQm5yoxTZAXEybztLVcURiTABHnuCxiMeqqtT0CSxjZBQFBBjAzkk2iTDVZAZCsioPN4yBTf4Xp859XaLgcLF1POxlnbep6lZCldg39QFKPZB2kEcChRh0yGyQIZALEZBAZCfwZDZD",
		},
		PROD: {
			appId: "654004691704493",
			access_token: "EAAYsfZAxiFmMBALGZCqEDuOEZCPS9eZC0BBTIZCQxXziAWZAQm5yoxTZAXEybztLVcURiTABHnuCxiMeqqtT0CSxjZBQFBBjAzkk2iTDVZAZCsioPN4yBTf4Xp859XaLgcLF1POxlnbep6lZCldg39QFKPZB2kEcChRh0yGyQIZALEZBAZCfwZDZD",
		},
	};


	/**
	 * Google API configurations
	 * 
	 * For more info:
	 * https://www.npmjs.com/package/spreadsheet-to-json
	 * 
	 * Sheet used in this example:
	 * https://docs.google.com/spreadsheets/d/1V8GPkVN879SspeG6Im3HWCAKsN_C7apYUdMjyb7AgU0/edit#gid=188598919
	 */
	configurationObject.GOOGLESHEET_CONFIG = {
		SPREADSHEET_KEY: "1V8GPkVN879SspeG6Im3HWCAKsN_C7apYUdMjyb7AgU0",
		CREDENTIALS_PATH: require(PATHS.BUILD_SCRIPTS_AND_CONFIG + "googleCredentials.json"),
		SHEETS_TO_EXTRACT: ["NPM TEST"]
	};

	// deploySettings: DEPLOY_CONFIG.DEV
	return configurationObject;
}());