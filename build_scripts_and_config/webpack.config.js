const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const buildTargets = require("./buildTargets");
const fs = require("fs");
const PROJECT_CONFIG = require("./project.config");
const webpack = require("webpack");

// absolute paths to all symlinked modules inside `nodeModulesPath`
// adapted from https://github.com/webpack/webpack/issues/811#issuecomment-405199263
const findLinkedModules = (nodeModulesPath) => {
	const modules = []

	fs.readdirSync(nodeModulesPath).forEach(dirname => {
		const modulePath = path.resolve(nodeModulesPath, dirname)
		const stat = fs.lstatSync(modulePath)

		if (dirname.startsWith('.')) {
			// not a module or scope, ignore
		} else if (dirname.startsWith('@')) {
			// scoped modules
			modules.push(...findLinkedModules(modulePath))
		} else if (stat.isSymbolicLink()) {
			const realPath = fs.realpathSync(modulePath)
			const realModulePath = path.resolve(realPath, 'node_modules')

			modules.push(realModulePath)
		}
	})

	return modules
}

module.exports = function (options) {

	let config = {

		mode: 'development',

		entry: {
			main: path.resolve(__dirname, PROJECT_CONFIG.PATHS.SRC + "index.ts")
		},

		output: {
			path: path.resolve(__dirname, PROJECT_CONFIG.PATHS.OUTPUT_PATH),
			filename: "bundle.js"
		},

		module: {
			rules: [{
				test: /\.ts$/,
				loader: "ts-loader"
			}]
		},

		plugins: [
			new HtmlWebpackPlugin({
				title: 'Coolgames',
				inject: true,
				template: path.resolve(__dirname, PROJECT_CONFIG.PATHS.SRC + 'index.html')
			}),

			new webpack.DefinePlugin({
				USE_MOCKS: options.useMock ? JSON.stringify(true) : JSON.stringify(false),
				DEV: JSON.stringify(options.target === buildTargets.DEV),
				STAGE: JSON.stringify(options.target === buildTargets.STAGE),
				PROD: JSON.stringify(options.target === buildTargets.PROD)
			})
		],

		resolve: {
			extensions: ['.ts', '.js', '.json'],
			modules: [
				// provide absolute path to the main node_modules,
				// to avoid webpack searching around and getting confused
				// see https://webpack.js.org/configuration/resolve/#resolve-modules
				path.resolve('node_modules'),
				// include linked node_modules as fallback, in case the deps haven't
				// yet propagated to the main node_modules
				...findLinkedModules(path.resolve('node_modules')),
			],
		}
	}

	if (options && options.target) {

		switch (options.target) {

			case buildTargets.DEV:

				config.mode = "development";
				config.devtool = 'source-map';

				config.devServer = {
					contentBase: path.resolve(__dirname, PROJECT_CONFIG.PATHS.OUTPUT_PATH),
					compress: true,
					port: 8080,
					https: {
						key: fs.readFileSync('./key.pem'),
						cert: fs.readFileSync('./cert.pem')
					}
				};
				break;
			case buildTargets.PROD:
				config.mode = "production";
				break;
			case buildTargets.STAGE:
				config.mode = "development";
				break;
			default:
				break;
		}
	}

	return config;
};