# CoolGames PIXI Game Boilerplate
---

_todo: automate setup_

## Project setup:
### 1. Install dependencies

For mac make sure you have homebrew installed by pasting the following in the terminal:

`/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`


#### ffmpeg
*mac:* 
`brew install ffmpeg`

(image magick installer also has an option to install ffmpeg with it if you want to kill two birds in one stone)

*windows:*
[Download and install](http://ffmpeg.org/download.html)

#### ImageMagick

*mac:*
`brew install imagemagick`

*windows:*

*IMPORTANT* Make sure that you check the checkmark to install the legacy utilities (e.g. convert) 
![alt text](https://drive.google.com/uc?export=view&id=1-jqGIlHhkfEy7oZfVvzs0NS2oEoREyJO)


[Download and install](https://imagemagick.org/download/binaries/ImageMagick-7.0.8-53-Q16-x64-dll.exe)


#### Node
*mac:*:
`brew install node`

*windows:*
[Download and install](https://nodejs.org/en/download/)

### 2. Install gulp-cli globally
`npm install gulp-cli -g`

### 3. Run: `npm install`
---

## Splitting the boilerplate off to start a game:

1. Create an empty repository for the new game.
2. cd to the boilerplate's root.
3. Add the new game's repo as a new remote

	`git remote add new-origin git@github.com:coolgames/newgame.git`
1. Push master to the new game's repo.

	`git push new-origin master`
1.	Remove the boilerplates' origin

	`git remote rm origin`
1. Rename the new-origin to origin.

	`git remote rename new-origin origin`

1. Adapt some of the boilerplate remnants. 
   * Change this readme file to match the game
   * Change version and name in package.json
   * Make necessary changes if to "./build_scripts_and_config/project.config.js" (for instance the target facebook app for deployment)

---

## Development:

The development flow is setup to work with VSCode. After finishing the setup described above press "F5" to start the webpack dev server and start working!

---

## Deploying:

The project has separate build targets setup:

`gulp deploy --dev`

`gulp deploy --staging`

`gulp deploy --prod`

---


## Known issues:

### SSH Key Permission denied (public key)
Possible solution: Add Host to known_host in ./ssh (Win)

`ssh HOSTNAME`

Examples: 

`ssh git.boostermediastudios.com`

`ssh bitbucket.org`

[Read more about it](https://confluence.atlassian.com/bbkb/permission-denied-publickey-302811860.html)

---

## Working with local npms (ie: when working on framework)

1. `cd path/to/local/npm`
1. (sudo) `npm link`
	npm creates local package -> output/name)
1. copy output/name
1. `cd path/to/other/project`
	(sudo) `npm link output/name`