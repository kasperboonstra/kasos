import { Cool } from "@coolgames/pixi-framework";

export interface IButtonEvent
{
	button: BanannaButton;
}

export enum BanannaButton
{
	UP,
	DOWN,
	CENTER,
	LEFT,
	RIGHT
}

// tslint:disable max-classes-per-file
export class ButtonEvent extends Cool.Event<IButtonEvent>{ }