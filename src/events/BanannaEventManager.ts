import { Cool } from "@coolgames/pixi-framework";
import { ButtonEvent } from "./ButtonEvents";
import { VoidEvent } from "./VoidEvent";

export class BannanaEventManager extends Cool.EventManager
{
	public static buttonClicked: ButtonEvent = new ButtonEvent();
	public static buttonDown: ButtonEvent = new ButtonEvent();
	public static buttonUp: ButtonEvent = new ButtonEvent();
}