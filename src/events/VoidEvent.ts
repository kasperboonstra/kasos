import { Cool } from "@coolgames/pixi-framework";

// tslint:disable-next-line: no-empty-interface
export interface IVoidEvent { }

export class VoidEvent extends Cool.Event<IVoidEvent>{ }