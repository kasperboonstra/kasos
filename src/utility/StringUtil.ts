export function FormatString(...stringParameters: string[]): string
{
	// The string containing the format items (e.g. "{0}")
	// will and always has to be the first argument.
	let theString: string = stringParameters[0];

	// start with the second argument (i = 1)
	for (let i: number = 1; i < stringParameters.length; i++)
	{
		// "gm" = RegEx options for Global search (more than one instance)
		// and for Multiline search
		let regEx: RegExp = new RegExp("\\{" + (i - 1) + "\\}", "gm");

		theString = theString.replace(regEx, stringParameters[i]);
	}

	return theString;
}