import { BannanaEventManager } from "./events/BanannaEventManager";
import { IButtonEvent, BanannaButton } from "./events/ButtonEvents";

class InputSingleton
{
	public static getInstance(): InputSingleton
	{
		if (!InputSingleton.instance)
		{
			InputSingleton.instance = new InputSingleton();
		}

		return InputSingleton.instance;
	}
	private static instance: InputSingleton;
	private _buttonDown: boolean = false;

	public get buttonDown(): boolean {
		return this._buttonDown;
	}

	constructor()
	{
		this._addEventListeners();
	}

	private _addEventListeners(): void
	{
		BannanaEventManager.buttonClicked.addListener(this._buttonClicked, this);
		BannanaEventManager.buttonDown.addListener(this._onButtonDown, this);
		BannanaEventManager.buttonUp.addListener(this._buttonUp, this);
	}

	private _buttonClicked(data: IButtonEvent): void
	{
		switch (data.button)
		{
			case BanannaButton.CENTER:
				console.log("CENTER BUTTON CLICKED Y'ALL");
				break;
			case BanannaButton.LEFT:
				console.log("LEFT BUTTON CLICKED Y'ALL");
				break;
			case BanannaButton.RIGHT:
				console.log("RIGHT BUTTON CLICKED Y'ALL");
				break;
			case BanannaButton.UP:
				console.log("UP BUTTON CLICKED Y'ALL");
				break;
			case BanannaButton.DOWN:
				console.log("DOWN BUTTON CLICKED Y'ALL");
				break;
		}
	}

	private _onButtonDown(data: IButtonEvent): void
	{
		this._buttonDown = true;
		switch (data.button)
		{
			case BanannaButton.CENTER:
				console.log("CENTER BUTTON DOWN Y'ALL");
				break;
			case BanannaButton.LEFT:
				console.log("LEFT BUTTON DOWN Y'ALL");
				break;
			case BanannaButton.RIGHT:
				console.log("RIGHT BUTTON DOWN Y'ALL");
				break;
			case BanannaButton.UP:
				console.log("UP BUTTON DOWN Y'ALL");
				break;
			case BanannaButton.DOWN:
				console.log("DOWN BUTTON DOWN Y'ALL");
				break;
		}
	}

	private _buttonUp(data: IButtonEvent): void
	{
		this._buttonDown = false;
		switch (data.button)
		{
			case BanannaButton.CENTER:
				console.log("CENTER BUTTON UP Y'ALL");
				break;
			case BanannaButton.LEFT:
				console.log("LEFT BUTTON UP Y'ALL");
				break;
			case BanannaButton.RIGHT:
				console.log("RIGHT BUTTON UP Y'ALL");
				break;
			case BanannaButton.UP:
				console.log("UP BUTTON UP Y'ALL");
				break;
			case BanannaButton.DOWN:
				console.log("DOWN BUTTON UP Y'ALL");
				break;
		}
	}
}

export const Input: InputSingleton = InputSingleton.getInstance();
