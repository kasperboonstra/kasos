import { Cool } from "@coolgames/pixi-framework";

export class Layers extends Cool.LayersCore
{
	// example
	public static MAIN: Cool.LayerName = new Cool.LayerName("main");
	public static POPUP: Cool.LayerName = new Cool.LayerName("popup");
	public static UI: Cool.LayerName = new Cool.LayerName("ui");
}