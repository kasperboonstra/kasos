import { Cool } from "@coolgames/pixi-framework";
import { Layers } from "../Layers";

export class ExamplePathCommand extends Cool.PathCommand implements Cool.IPathCommand
{

	public constructor()
	{
		// ExamplePathCommand::ExamplePathCommand
		super();

		console.log("ExamplePathCommand::ExamplePathCommand");
	}

	public execute(path: Cool.PathName, options: Object): void
	{
		console.log("ExamplePathCommand.execute");

		options = {
			x: 100,
			y: 100,
		};

		// let layer: Cool.Layer = Cool.LayerManager.getLayer(Layers.MAIN); // .add(View);
		// layer.add(HomeScreen, options);
	}
}