import { Cool } from "@coolgames/pixi-framework";
import { SplashScreen } from "../view_examples/SplashScreen";
import { Layers } from "../Layers";
import { Paths } from "../Paths";
import { GameScreen } from "../view_examples/GameScreen";

export class ExamplePathMap extends Cool.AbstractPathMap
{
	protected _paths: Map<Cool.PathName, Function> = new Map([
		[Paths.SPLASH, this._splash],
		[Paths.GAME, this._game]
	]);

	public constructor()
	{
		super();
	}

	private _splash(options: object): void
	{
		console.log("ExamplePathMap.splash");

		// use ViewManager directly:
		// Cool.ViewManager.showView(SplashScreen, {}, Layers.MAIN);
		// or shortcut:

		// this.showView(view, obj)

		this.showView(SplashScreen, {}, Layers.MAIN);
	}

	private _game(options: object): void
	{
		console.log("ExamplePathMap.splash");

		// use ViewManager directly:
		// Cool.ViewManager.showView(SplashScreen, {}, Layers.MAIN);
		// or shortcut:

		// this.showView(view, obj)

		this.showView(GameScreen, {}, Layers.MAIN);
	}
}