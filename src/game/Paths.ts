import { Cool } from "@coolgames/pixi-framework";

export class Paths extends Cool.PathsCore
{
	public static SPLASH: Cool.PathName = new Cool.PathName("splash");
	public static HOME: Cool.PathName = new Cool.PathName("home");
	public static GAME: Cool.PathName = new Cool.PathName("game");
	public static LOBBY: Cool.PathName = new Cool.PathName("lobby");
	public static TEST: Cool.PathName = new Cool.PathName("test");

	public static MENU: Cool.PathName = new Cool.PathName("menupopup");
}