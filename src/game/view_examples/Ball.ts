import { GameScreen } from "./GameScreen";

import { AssetImage, LocalizedText } from "../../assetmap/AssetMap";
import { Cool } from "@coolgames/pixi-framework";
import { TweenLite, Power2 } from "gsap";

export class Ball
{

		public x: number = 0;
		public y: number = 0;
		public vy: number = 0;
		public vx: number = 0;
		public asset: PIXI.Sprite;
		public screen: GameScreen;

		public doNotCollisionCheckMePlease: boolean = false;

		private _shouldUpdate: boolean = true;

		constructor(screen: GameScreen)
		{
				this.screen = screen;
				let list: any[] = [AssetImage.BALL_BLUE, AssetImage.BALL_GREEN, AssetImage.BALL_PINK, AssetImage.BALL_YELLOW ];
				this.asset = Cool.AssetManager.getSprite(list[Math.floor(Math.random() * list.length)]);

				this.x = 120;
				this.y = 60;
				this.vx = -.4 + (Math.random() * .8);

				this.asset.y = this.y;
				this.asset.x = this.x;
				this.asset.anchor.set(0.5, 0.5);

				this.screen.addChild(this.asset);
		}

		public animateTo(x: number, y: number, cup: PIXI.Sprite): void
		{

				TweenLite.to(this.asset.scale, 0.09, {
				     x: 0, y: 0, ease: Power2.easeIn, delay: 0.03
				 });

				TweenLite.to(this.asset, .1, {
					x , y: 0, ease: Power2.easeIn
				});
				// TweenLite.to(this.asset.scale, 0.6, {
				//     x: 1.5, y: 1.5, ease: Power2.easeIn
				// });
				this.vx = 0;
			 this.vy *= .4;
				this._shouldUpdate = false;
		}

		public update(): void
		{
				/*if (this._shouldUpdate)
				{*/
						this.vy += .7;
						this.vy * .9;
						this.vx * .9;
						this.y += this.vy;
						this.x += this.vx;
						this.asset.y = this.y;
						this.asset.x = this.x;
				/*}*/
		}

}