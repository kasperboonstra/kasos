export class SpriteOverlap
{
		public static isOverlapping(s2: PIXI.Sprite, s1: PIXI.Sprite): boolean
		{

			if (s1.x > s2.x - s2.width / 2 * .9)
			{
				if (s1.x < s2.x + s2.width / 2 * .9)
				{
					if (s1.y > s2.y - s2.height * .6)
					{
						if (s1.y < s2.y + s2.height * .6)
						{

					return true;
						}
					}

				}
			}

				/* if ((s1.x - s1.width / 2) + (s1.width / 2) > (s2.x - s2.width / 2))
				{
						if ((s1.x - s1.width / 2) < (s2.x - s2.width / 2) + (s2.width / 2))
						{
								if ((s1.y - s1.height / 2) + (s1.height / 2) > (s2.y - s2.height / 2))
								{
										if ((s1.y - s1.height / 2) < (s2.y - s2.height / 2) + (s2.height / 2))
										{
												return true;
										}
								}
						}
				}*/

			return false;
		}
}