
import { Cool } from "@coolgames/pixi-framework";
import { Input } from "../../Input";
import { Groups } from "../Groups";
import { AssetImage } from "../../assetmap/AssetMap";
import { Ball } from "./Ball";
import { SpriteOverlap } from "./SpriteOverlap";

// Enum with all possible states
enum STATES
{
	START_SCREEN,
	LEVEL_START,
	PLAYING,
	LEVEL_COMPLETE,
	LEVEL_FAIL
}

export class GameScreen extends Cool.View implements Cool.IView, Cool.IUpdateable
{
	public static projectConfig: { levels: any[] } = {
		levels: [
			0,
			{
				balls: 50,
				goal: 25,
				spaceBetween: 50,
				moveSpeed: -1
			},
			{
				balls: 75,
				goal: 50,
				spaceBetween: 50,
				moveSpeed: 1
			},
			{
				balls: 50,
				goal: 50,
				spaceBetween: 60,
				moveSpeed: -1.25
			},
			{
				balls: 75,
				goal: 50,
				spaceBetween: 70,
				moveSpeed: 1.5
			},
			{
				balls: 75,
				goal: 75,
				spaceBetween: 60,
				moveSpeed: -1.5
			},
			{
				balls: 100,
				goal: 100,
				spaceBetween: 60,
				moveSpeed: -1.25
			},
		]
	};

	private _cupList: PIXI.Sprite[];

	private _placeholderText: PIXI.Text;
	private _previousState: number;
	private _gameState: number;
	private _score: number;
	private _currentLevel: number;
	private _ballTimer: number;
	private _ballInterval: number;
	private _ballsLeft: number;

	private _topSprite: PIXI.Sprite;
	private _progressEmptySprite: PIXI.Sprite;
	private _progressFullSprite: PIXI.Sprite;
	private _progressLeftBlock: PIXI.Sprite;
	private _progressRightBlock: PIXI.Sprite;
	private _startButton: PIXI.Sprite;
	private _startButtonPressed: PIXI.Sprite;
	private _backgroundSprite: PIXI.Sprite;
	private _ballCounter: PIXI.Sprite;
	private _retryButton: PIXI.Sprite;
	private _retryButtonPressed: PIXI.Sprite;

	private _levelText: PIXI.Text;
	private _objectiveText: PIXI.Text;
	private _scoreText: PIXI.Text;
	private _levelCompleteText: PIXI.Text;
	private _ballCounterText: PIXI.Text;
	private _levelFailedText: PIXI.Text;
	private _levelLeftText: PIXI.Text;
	private _levelRightText: PIXI.Text;

	private countdown: number = 10;
	private ballz: any[] = [];

	private failTimer: number = 0;
	private failTime: number = 2000;

	public constructor(options: Cool.IViewSettings)
	{
		super(options);
		console.log("SplashScreen::SplashScreen", options);
		this.position.set(-Cool.GameCanvas.screen.width / 2, -Cool.GameCanvas.screen.height / 2);
	}
	public initialize(onInitialized: Function): void
	{
		console.log("Method not implemented.");

		this._score = 0;
		this._currentLevel = 1;

		this._ballInterval = 60;
		this._ballTimer = 0;

		onInitialized();
	}
	public showImmediate(): void
	{
		console.log("Method not implemented.");
	}
	public resize(data: Cool.IResizeEvent): void
	{
		console.log("Method not implemented.");
	}

	public onDestroy(): void
	{
		console.warn("Method not implemented.");
	}

	public show(): void
	{
		// console.log("SplashScreen::show");
		this._setup();
		Groups.Updateables.add(this);
	}

	public hide(): void
	{
		Groups.Updateables.remove(this);
	}

	public update(deltaTime: number, elapsedMS: number): void
	{
		for (let i: number = 0; i < this.ballz.length; i++)
		{
			let current: Ball = this.ballz[i];
			if (current.doNotCollisionCheckMePlease)
			{

			current.update();
			continue;
						}
			current.update();

			this._cupList.forEach(sprite =>
						{
								if (SpriteOverlap.isOverlapping(sprite, current.asset))
								{
										current.doNotCollisionCheckMePlease = true;
										current.animateTo(sprite.x, sprite.y, sprite);
										this._setScore(this._score + 1);
								}
						});
		}

		// Update ticker
		switch (this._gameState)
		{
			case STATES.START_SCREEN: {
				// statements;
				if (Input.buttonDown)
				{
					// start game
							// Remove start button if it exists
					this._startButtonClicked();
					this._changeState(STATES.LEVEL_START);
				}
				break;
			}
			case STATES.PLAYING: {
				// statements;
				this._ballTimer -= elapsedMS;

				if (Input.buttonDown)
				{
					// Drop balls
					if (this._ballTimer <= 0) {
						this._ballTimer = this._ballInterval;
						let ball: Ball = new Ball(this);
						this._decreaseBallCounter();
						this.ballz.push(ball);
					}
				}

				this._moveCups(deltaTime);
				break;
			}
			case STATES.LEVEL_FAIL: {
				// statements;
				this.failTimer += elapsedMS;
				if (this.failTimer > this.failTime && Input.buttonDown)
				{
					this._retryButtonClicked();
					this._changeState(STATES.LEVEL_START);
					this.failTimer = 0;
				}
				break;
			}
			default: {
				// statements;
				break;
			}
		}
	}

	private _createCups(): void{
		// Create cups
		let spaceBetween: number = GameScreen.projectConfig.levels[this._currentLevel].spaceBetween;
		let cupAmount: number = Math.round(240 / 52);
		this._cupList = [];

		for (let i = 0; i < cupAmount; i++) {
			let cup: PIXI.Sprite = Cool.AssetManager.getSprite(AssetImage.CUP);
			cup.anchor.set(0.5, 0.5);
			cup.x = i * cup.width + cup.width / 2 + i * spaceBetween;
			cup.y = 270;
			cup.interactive = true;
			cup.buttonMode = false;
			this.addChild(cup);
			this._cupList.push(cup);
		}
	}

	private _moveCups(deltaTime: number): void{
		// move the cups here
		let spaceBetween: number = GameScreen.projectConfig.levels[this._currentLevel].spaceBetween;
		let moveSpeed: number = GameScreen.projectConfig.levels[this._currentLevel].moveSpeed;
		let cupAmount: number = Math.round(240 / 52);

		for (let i = 0; i < cupAmount; i++) {
			let cup: PIXI.Sprite = this._cupList[i];
			cup.x += moveSpeed * deltaTime;

			if (moveSpeed < 0) {
				if (cup.x < 0 - cup.width) {
					let leftCup: number = i - 1;

					if (leftCup < 0) {
						leftCup = cupAmount - 1;
					}

					let leftCupPosition: number = this._cupList[leftCup].x;
					cup.x = leftCupPosition + cup.width + spaceBetween;
				}
			} else {
				if (cup.x > 240 + cup.width) {
					let rightCup: number = i + 1;

					if (rightCup >= cupAmount) {
						rightCup = 0;
					}

					let rightCupPosition: number = this._cupList[rightCup].x;
					cup.x = rightCupPosition - cup.width - spaceBetween;
				}
			}
		}
	}

	private _createProgressBar(): void
	{
		// Remove progress bar if it exists
		// Create progress bar with level x to y
		// Set values to 0

		if (!this._progressEmptySprite) {
			this._progressEmptySprite = Cool.AssetManager.getSprite(AssetImage.PROGRESS_EMPTY);
			this._progressEmptySprite.anchor.set(0, 0.5);
			this._progressEmptySprite.x = 120 - this._progressEmptySprite.width / 2;
			this._progressEmptySprite.y = 42;
			this._progressEmptySprite.interactive = false;
			this._progressEmptySprite.buttonMode = false;
			this.addChild(this._progressEmptySprite);
		}

		if (!this._progressFullSprite) {
			this._progressFullSprite = Cool.AssetManager.getSprite(AssetImage.PROGRESS_FILLED);
			this._progressFullSprite.anchor.set(0, 0.5);
			this._progressFullSprite.x = 120 - this._progressEmptySprite.width / 2;
			this._progressFullSprite.y = 42;
			this._progressFullSprite.interactive = false;
			this._progressFullSprite.buttonMode = false;
			this.addChild(this._progressFullSprite);
		}

		if (!this._progressLeftBlock) {
			this._progressLeftBlock = Cool.AssetManager.getSprite(AssetImage.PROGRESS_BLOCK);
			this._progressLeftBlock.anchor.set(1, 0.5);
			this._progressLeftBlock.x = 120 - this._progressEmptySprite.width / 2;
			this._progressLeftBlock.y = 42;
			this._progressLeftBlock.interactive = false;
			this._progressLeftBlock.buttonMode = false;
			this.addChild(this._progressLeftBlock);
		}

		if (!this._progressRightBlock) {
			this._progressRightBlock = Cool.AssetManager.getSprite(AssetImage.PROGRESS_BLOCK);
			this._progressRightBlock.anchor.set(0, 0.5);
			this._progressRightBlock.x = 120 + this._progressEmptySprite.width / 2;
			this._progressRightBlock.y = 42;
			this._progressRightBlock.interactive = false;
			this._progressRightBlock.buttonMode = false;
			this.addChild(this._progressRightBlock);
		}

		if (!this._levelLeftText) {
			const textStyle: PIXI.TextStyle = new PIXI.TextStyle({
				fontFamily: "Russo_One",
				align: "center",
				fontSize: 20,
				fill: "#000000"
			});

			this._levelLeftText = new PIXI.Text("", textStyle);
			this._levelLeftText.anchor.set(1, 0.5);
			this._levelLeftText.x = this._progressLeftBlock.x - 5;
			this._levelLeftText.y = this._progressLeftBlock.y;
			this.addChild(this._levelLeftText);
		}

		if (!this._levelRightText) {
			const textStyle: PIXI.TextStyle = new PIXI.TextStyle({
				fontFamily: "Russo_One",
				align: "center",
				fontSize: 20,
				fill: "#000000"
			});

			this._levelRightText = new PIXI.Text("", textStyle);
			this._levelRightText.anchor.set(0, 0.5);
			this._levelRightText.x = this._progressRightBlock.x + 4;
			this._levelRightText.y = this._progressRightBlock.y;
			this.addChild(this._levelRightText);
		}

		this._progressFullSprite.scale.x = 0;
		this._levelLeftText.text = this._currentLevel.toString();
		this._levelRightText.text = (this._currentLevel + 1).toString();

	}

	private _updateProgressBar(): void
	{
		// if progress bar exists..
		if (this._gameState === STATES.PLAYING && this._progressFullSprite) {
			let percentage: number =  this._score / GameScreen.projectConfig.levels[this._currentLevel].goal.toString();
			this._progressFullSprite.scale.x = percentage;

			if (percentage >= 1) {
				this._progressFullSprite.scale.x = Math.min(percentage, 1);
				this._changeState(STATES.LEVEL_COMPLETE);
			}
		}

		// set value to the new value (scale, no easing)
		// if progress is 100, change state to game complete
	}

	private _createScore(): void
	{
		// Remove score
		// create score UI
		// Set score to 0

		if (!this._scoreText)
		{
			const textStyle: PIXI.TextStyle = new PIXI.TextStyle({
				fontFamily: "Russo_One",
				align: "center",
				fontSize: 32,
				fill: "#FFFFFF"
			});

			this._scoreText = new PIXI.Text("", textStyle);
			this._scoreText.anchor.set(0.5, 0.5);
			this._scoreText.x = Cool.GameCanvas.scaleValue(120);
			this._scoreText.y = Cool.GameCanvas.scaleValue(22);
			this.addChild(this._scoreText);
		}

		this._score = 0;
		this._scoreText.text = this._score.toString();
		this._scoreText.alpha = 1;

	}

	private _setScore(newScore: number): void
	{
		// check if score UI exists, if not create it
		// set value to new value
		this._score = newScore;
		this._scoreText.text = newScore.toString();
		this._updateProgressBar();
	}

	private _createBallCounter(): void
	{
		if (!this._ballCounter) {
			this._ballCounter = Cool.AssetManager.getSprite(AssetImage.BALLCOUNTER);
			this._ballCounter.anchor.set(0, 0);
			this._ballCounter.x = 130;
			this._ballCounter.y = 55;
			this._ballCounter.interactive = false;
			this._ballCounter.buttonMode = false;
			this.addChild(this._ballCounter);
		}

		if (!this._ballCounterText) {
			const textStyle: PIXI.TextStyle = new PIXI.TextStyle({
				fontFamily: "Russo_One",
				align: "center",
				fontSize: 14,
				fill: "#8cd9f4"
			});

			this._ballCounterText = new PIXI.Text("", textStyle);
			this._ballCounterText.anchor.set(0, 0);
			this._ballCounterText.x = 132;
			this._ballCounterText.y = 55;
			this.addChild(this._ballCounterText);
		}

		this._ballsLeft = GameScreen.projectConfig.levels[this._currentLevel].balls;
		this._ballCounterText.text = this._ballsLeft.toString();

		// Remove ball counter
		// create ball counter
		// Set ball counter to level value
	}

	private _decreaseBallCounter(): void
	{
		// decrease ball counter by 1
		// If it's below 1, change state to game over (when balls are away)
		this._ballsLeft--;
		if (this._ballsLeft < 0) {
			this._ballsLeft = 0;
		}
		this._ballCounterText.text = this._ballsLeft.toString();

		let self = this;

		setTimeout(function() {
			if (self._ballsLeft <= 0 && self._gameState === STATES.PLAYING) {
				self._changeState(STATES.LEVEL_FAIL);
			}
		}, 4000);

	}

	private _startButtonClicked(): void
	{
		if (this._startButton) {
			this._startButton.alpha = 0;
			this._startButtonPressed.alpha = 1;

			let self = this;

			setTimeout(function() {
				self._startButtonPressed.alpha = 0;
			}, 100);
		}
	}

	private _retryButtonClicked(): void
	{
		if (this._retryButton) {
			this._retryButton.alpha = 0;
			this._retryButtonPressed.alpha = 1;

			let self = this;

			setTimeout(function() {
				self._retryButtonPressed.alpha = 0;
			}, 100);
		}
	}

	private _showStartButton(): void
	{
		// Create start button if it doesn't exist
		// set alpha of start button on 1
		if (!this._startButton) {
			this._startButton = Cool.AssetManager.getSprite(AssetImage.BUTTON_START);
			this._startButton.anchor.set(0.5, 0.5);
			this._startButton.x = 120;
			this._startButton.y = 240;
			this._startButton.interactive = true;
			this._startButton.buttonMode = true;
			this.addChild(this._startButton);
		}

		if (!this._startButtonPressed) {
			this._startButtonPressed = Cool.AssetManager.getSprite(AssetImage.BUTTON_START_DOWN);
			this._startButtonPressed.anchor.set(0.5, 0.5);
			this._startButtonPressed.x = 120;
			this._startButtonPressed.y = 240;
			this._startButtonPressed.interactive = true;
			this._startButtonPressed.buttonMode = true;
			this._startButtonPressed.alpha = 0;
			this.addChild(this._startButtonPressed);
		}

		this._startButton.alpha = 1;
	}

	private _showRetryButton(): void
	{
		// Create start button if it doesn't exist
		// set alpha of start button on 1
		if (!this._retryButton) {
			this._retryButton = Cool.AssetManager.getSprite(AssetImage.BUTTON_RETRY);
			this._retryButton.anchor.set(0.5, 0.5);
			this._retryButton.x = 120;
			this._retryButton.y = 240;
			this._retryButton.interactive = true;
			this._retryButton.buttonMode = true;
			this.addChild(this._retryButton);
		}

		if (!this._retryButtonPressed) {
			this._retryButtonPressed = Cool.AssetManager.getSprite(AssetImage.BUTTON_RETRY_DOWN);
			this._retryButtonPressed.anchor.set(0.5, 0.5);
			this._retryButtonPressed.x = 120;
			this._retryButtonPressed.y = 240;
			this._retryButtonPressed.interactive = true;
			this._retryButtonPressed.buttonMode = true;
			this._retryButtonPressed.alpha = 0;
			this.addChild(this._retryButtonPressed);
		}

		this._retryButton.alpha = 1;
	}

	private _showLevelText(): void
	{
		// create level text if it doesn't exist
		// set alpha of level text to 1

		if (!this._levelText)
		{
			const textStyle: PIXI.TextStyle = new PIXI.TextStyle({
				fontFamily: "Russo_One",
				align: "center",
				fontSize: 16
			});

			this._levelText = new PIXI.Text("", textStyle);
			this._levelText.anchor.set(0.5, 0.5);
			this.addChild(this._levelText);
		}

		this._levelText.text = "Level " + this._currentLevel.toString();

		this._levelText.x = Cool.GameCanvas.scaleValue(120);
		this._levelText.y = Cool.GameCanvas.scaleValue(140);

		this._levelText.alpha = 1;
	}

	private _showObjectiveText(): void
	{
		// set alpha of start button on 1
		// If it's below 1, change state to game over

		if (!this._objectiveText)
		{
			const textStyle: PIXI.TextStyle = new PIXI.TextStyle({
				fontFamily: "Russo_One",
				align: "center",
				fontSize: 20
			});

			this._objectiveText = new PIXI.Text("", textStyle);
			this._objectiveText.anchor.set(0.5, 0.5);
			this.addChild(this._objectiveText);
		}

		this._objectiveText.text = GameScreen.projectConfig.levels[this._currentLevel].goal.toString() + " Balls";

		this._objectiveText.x = Cool.GameCanvas.scaleValue(120);
		this._objectiveText.y = Cool.GameCanvas.scaleValue(159);

		this._objectiveText.alpha = 1;
	}

	private _showLevelCompleteText(): void
	{
		// set alpha of start button on 1
		// If it's below 1, change state to game over

		if (!this._levelCompleteText)
		{
			const textStyle: PIXI.TextStyle = new PIXI.TextStyle({
				fontFamily: "Russo_One",
				align: "center",
				fontSize: 20
			});

			this._levelCompleteText = new PIXI.Text("", textStyle);
			this._levelCompleteText.anchor.set(0.5, 0.5);
			this.addChild(this._levelCompleteText);
		}

		this._levelCompleteText.text = "COMPLETE";

		this._levelCompleteText.x = Cool.GameCanvas.scaleValue(120);
		this._levelCompleteText.y = Cool.GameCanvas.scaleValue(159);

		this._levelCompleteText.alpha = 1;
	}

	private _showLevelFailedText(): void
	{
		// set alpha of start button on 1
		// If it's below 1, change state to game over

		if (!this._levelFailedText)
		{
			const textStyle: PIXI.TextStyle = new PIXI.TextStyle({
				fontFamily: "Russo_One",
				align: "center",
				fontSize: 20
			});

			this._levelFailedText = new PIXI.Text("", textStyle);
			this._levelFailedText.anchor.set(0.5, 0.5);
			this.addChild(this._levelFailedText);
		}

		this._levelFailedText.text = "FAILED";

		this._levelFailedText.x = Cool.GameCanvas.scaleValue(120);
		this._levelFailedText.y = Cool.GameCanvas.scaleValue(159);

		this._levelFailedText.alpha = 1;
	}

	private _showStartScreen(): void
	{
		// show start screen ui elements

		// Level count
		this._showLevelText();

		// Level objective
		this._showObjectiveText();

		// Score UI to 0
		this._createScore();

		// Create progress bar
		this._createProgressBar();

		// Start button
		this._showStartButton();
	}

	private _startCupMovement(): void {
		// set cups alpha to 1
		// start movement of cups
	}

	private _startLevel(): void
	{
		// show start screen ui elements

		if (this._retryButton){
			this._retryButton.alpha = 0;
			this._retryButtonPressed.alpha = 0;
		}

		if (this._levelFailedText) {
			this._levelText.alpha = 0;
			this._levelFailedText.alpha = 0;
		}

		// Score UI to 0
		this._createScore();

		// Set progress bar
		this._createProgressBar();

		// Level count
		this._showLevelText();

		// Level objective
		this._showObjectiveText();

		// Ball counter
		this._createBallCounter();

		// after 2s:
		let self = this;
		setTimeout(function() {
			// Remove level text
			// Remove objective text
			// Change state to playing
			// Start cup movement and set alpha to 1
			self._levelText.alpha = 0;
			self._objectiveText.alpha = 0;
			self._changeState(STATES.PLAYING);
		}, 2000);
	}

	private _showFailScreen(): void
	{
		// show fail screen ui elements

		// Show level text
		this._showLevelText();

		// Show level text
		this._showLevelFailedText();

		// Show level text
		this._showRetryButton();

		// fade out cups
		// Show level text
		// Show failed text
		// Show retry button
		// retry calls startLevel
	}

	private _showCompleteScreen(): void
	{
		// show level complete screen

		// Show level text
		this._showLevelText();

		// Show complete text
		this._showLevelCompleteText();

		this._gameState = 10;

		// after 2s
		let self = this;
		setTimeout(function() {
			// increase level
			// Set score to 0
			// Hide complete text
			// Change state to start level
			self._levelText.alpha = 0;
			self._levelCompleteText.alpha = 0;
			self._currentLevel++;
			self._changeState(STATES.LEVEL_START);
		}, 2000);
	}

	private _changeState(newState: number): void
	{
		switch (newState)
		{
			case STATES.START_SCREEN: {
				// statements;
				this._showStartScreen();
				break;
			}
			case STATES.LEVEL_START: {
				this._startLevel();
				break;
			}
			case STATES.PLAYING: {
				// statements;
				this._startCupMovement();
				break;
			}
			case STATES.LEVEL_FAIL: {
				// statements;
				this._showFailScreen();
				break;
			}
			case STATES.LEVEL_COMPLETE: {
				// statements;
				this._showCompleteScreen();
				break;
			}
			default: {
				// statements;
				break;
			}
		}

		this._previousState = this._gameState;
		this._gameState = newState;
	}

	private _setup(): void
	{
		// const textStyle: PIXI.TextStyle = new PIXI.TextStyle({ fontFamily: "Russo_One" });
		// let text: PIXI.Text = new PIXI.Text("", textStyle);
		// text.text = "GAME SCREEN";
		// text.anchor.set(0.5, 0.5);
		// // text.y += Cool.GameCanvas.scaleValue(250);
		// this._placeholderText = text;
		// this.addChild(text);

		// add background
		this._backgroundSprite = Cool.AssetManager.getSprite(AssetImage.BACKGROUND);
		this._backgroundSprite.interactive = false;
		this._backgroundSprite.buttonMode = false;
		this.addChild(this._backgroundSprite);

		// add top UI
		this._topSprite = Cool.AssetManager.getSprite(AssetImage.TOP);
		this._topSprite.interactive = false;
		this._topSprite.buttonMode = false;
		this.addChild(this._topSprite);

		this._createCups();

		// this._logo.alpha = 0;
		// this._logo.y -= 100;
		// this._logo.scale.set(0, 0);
		// this._logo.anchor.set(0.5, 0.5);

		this._changeState(STATES.START_SCREEN);
	}
}