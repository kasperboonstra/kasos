
import { TweenLite, Power4, Power2, Back } from "gsap";

import { Cool } from "@coolgames/pixi-framework";
import { AssetImage, LocalizedText } from "../../assetmap/AssetMap";
import { Paths } from "../Paths";
export class SplashScreen extends Cool.View implements Cool.IView
{
	private _logo: PIXI.Sprite;
	private _loadingText: PIXI.Text;

	public constructor(options: Cool.IViewSettings)
	{
		console.log("SplashScreen::SplashScreen", options);

		super(options);

		this._setup();
	}

	public initialize(onInitialized: Function): void
	{
		console.log("Method not implemented.");

		onInitialized();
	}
	public showImmediate(): void
	{
		console.log("Method not implemented.");
	}
	public resize(data: Cool.IResizeEvent): void
	{
		console.log("Method not implemented.");
	}

	public onDestroy(): void
	{
		console.warn("Method not implemented.");
	}

	public show(): void
	{
		// console.log("SplashScreen::show");

		TweenLite.to(this._logo, 0.1, { alpha: 1, ease: Power4.easeIn, delay: 1.5 });

		TweenLite.to(this._logo, 0.6, {
			y: this._logo.y + 100, ease: Power2.easeOut, delay: 1.4, onStart: () =>
			{
				this.removeChild(this._loadingText);
			}
		});

		TweenLite.to(this._logo.scale, 0.35, { x: 0.5, delay: 1.5, ease: Back.easeOut });
		TweenLite.to(this._logo.scale, 0.4, { y: 0.5, delay: 1.6, ease: Back.easeOut });
		Cool.ViewManager.goTo(Paths.GAME, {});

	}

	public hide(hideComplete: Function): void
	{
		TweenLite.to(this._logo.scale, 0.4, {
			y: 0, delay: 4, ease: Back.easeOut, onComplete: () =>
			{
				hideComplete();
			}
		});
	}

	private _setup(): void
	{
	let sprite: PIXI.Sprite = Cool.AssetManager.getSprite(AssetImage.LOGO_COOLGAMES);

	sprite.interactive = true;
	sprite.buttonMode = true;

	this._logo = sprite;

	this.addChild(sprite);

	this._logo.alpha = 0;
	this._logo.y -= 100;
	this._logo.scale.set(0, 0);
	this._logo.anchor.set(0.5, 0.5);

	const textStyle: PIXI.TextStyle = new PIXI.TextStyle({ fontFamily: "Russo_One" });
	let text: PIXI.Text = new PIXI.Text("", textStyle);
	text.text = Cool.LocalizationManager.getText(LocalizedText.SPLASH_LOADING);
	text.anchor.set(0.5, 0.5);
	text.y += Cool.GameCanvas.scaleValue(250);
	this._loadingText = text;
	this.addChild(text);
	}
}