import "reflect-metadata";

import { PIXI } from "@coolgames/pixi-utilities";
import { FBMessagingPlatformService, FBMessagingPlatformServiceSettings } from "@coolgames/coolgames-messagingplatform-fbinstant";
// import { IMessagingPlatformService } from "@coolgames/coolgames-messagingplatform-core";
// import { MockMessagingPlatformService, MockMessagingPlatformServiceSettings } from "@coolgames/coolgames-messagingplatform-mock";
import { AssetAtlas, AssetFont, SupportedLanguage, AssetAudioSprite } from "./assetmap/AssetMap";
// import { ExamplePathMap, PATH } from "./game/path_examples/ExamplePathMap";
import { Groups } from "./game/Groups";
import { Layers } from "./game/Layers";
import { Cool } from "@coolgames/pixi-framework";
import { ExamplePathMap } from "./game/path_examples/ExamplePathMap";
import { Paths } from "./game/Paths";
import { BannanaEventManager } from "./events/BanannaEventManager";
import { BanannaButton } from "./events/ButtonEvents";
import { Input } from "./Input";

class Main
{
	constructor()
	{
		// this.initializeMessagingPlatform();
		this.loadDefaultAssets();
		Cool.LocalizationManager.setLanguage(SupportedLanguage.EN_US);
	}

	public loadDefaultAssets(): void
	{
		Cool.AssetLoader.initialize().then(() =>
		{
			Cool.AssetLoader.load(
				[AssetAtlas.DEFAULT],
				[AssetAudioSprite.DEFAULT],
				null,
				[AssetFont.QUANTIFY, AssetFont.RUSSO_ONE]
			).then(() =>
			{
				this.onLoadComplete();
			}
			).catch((e) =>
			{
				throw e;
			});
		});
	}

	public initGameLoop(): void
	{
		window.addEventListener("contextmenu", event => event.preventDefault());

		PIXI.ticker.shared.add((delta: number) =>
		{
			this.update(delta);
		});
	}

	private onLoadComplete(): void
	{
		this.initializeBannannaMode();
		this.initializeViewManager();
		this.showGame();
	}

	private initializeBannannaMode(): void
	{
		Cool.GameCanvas.setFixedCanvasSize(240, 320);

		let btnLeft: HTMLElement = document.createElement("BUTTON");
		btnLeft.innerHTML = "LEFT";
		document.body.appendChild(btnLeft);

		let btnRight: HTMLElement = document.createElement("BUTTON");
		btnRight.innerHTML = "RIGHT";
		document.body.appendChild(btnRight);

		let btnUp: HTMLElement = document.createElement("BUTTON");
		btnUp.innerHTML = "UP";
		document.body.appendChild(btnUp);

		let btnDown: HTMLElement = document.createElement("BUTTON");
		btnDown.innerHTML = "DOWN";
		document.body.appendChild(btnDown);

		let btnCenter: HTMLElement = document.createElement("BUTTON");
		btnCenter.innerHTML = "CENTER";
		document.body.appendChild(btnCenter);

		btnLeft.style.position = btnCenter.style.position = btnRight.style.position = btnDown.style.position = btnUp.style.position = "absolute";
		btnLeft.style.width = btnCenter.style.width = btnRight.style.width = btnDown.style.width = btnUp.style.width = "70px";

		btnLeft.style.left = btnCenter.style.left = btnRight.style.left = btnDown.style.left = btnUp.style.left = (window.innerWidth / 2) - 35 + "px";

		btnUp.style.top = 350 + "px";

		btnLeft.style.top = btnRight.style.top = "385px";
		btnCenter.style.top = "370px";
		btnCenter.style.height = "50px";

		btnDown.style.top = "420px";

		btnLeft.style.transform = "rotate(-90deg)";
		btnLeft.style.left = (window.innerWidth / 2) - 105 + 24 + "px";

		btnRight.style.left = (window.innerWidth / 2) + 35 - 24 + "px";
		btnRight.style.transform = "rotate(90deg)";

		btnCenter.onmousedown = () =>
		{
			BannanaEventManager.buttonDown.fire({
				button: BanannaButton.CENTER
			});
		};

		btnCenter.onmouseup = () =>
		{
			BannanaEventManager.buttonUp.fire({
				button: BanannaButton.CENTER
			});
		};

		btnCenter.onclick = () =>
		{
			BannanaEventManager.buttonClicked.fire({
				button: BanannaButton.CENTER
			});
		};

		btnDown.onclick = () =>
		{
			BannanaEventManager.buttonClicked.fire({
				button: BanannaButton.DOWN
			});
		};

		btnUp.onclick = () =>
		{
			BannanaEventManager.buttonClicked.fire({
				button: BanannaButton.UP
			});
		};

		btnLeft.onclick = () =>
		{
			BannanaEventManager.buttonClicked.fire({
				button: BanannaButton.LEFT
			});
		};

		btnRight.onclick = () =>
		{
			BannanaEventManager.buttonClicked.fire({
				button: BanannaButton.RIGHT
			});
		};

		window.addEventListener(
			"keydown", (key) =>
			{
				console.log(key);
				switch (key.code)
				{
					case "Space":
						BannanaEventManager.buttonClicked.fire({
							button: BanannaButton.CENTER
						});
						break;
					case "ArrowLeft":
						BannanaEventManager.buttonClicked.fire({
							button: BanannaButton.LEFT
						});
						break;
					case "ArrowRight":
						BannanaEventManager.buttonClicked.fire({
							button: BanannaButton.RIGHT
						});
						break;
					case "ArrowUp":
						BannanaEventManager.buttonClicked.fire({
							button: BanannaButton.UP
						});
						break;
					case "ArrowDown":
						BannanaEventManager.buttonClicked.fire({
							button: BanannaButton.DOWN
						});
						break;
					default:
						break;
				}
			}, false
		);
	}

	/**
	 * setupScreenFlow
	 */
	private initializeViewManager(): void
	{
		let containerMain: PIXI.Container = new PIXI.Container();
		let containerUI: PIXI.Container = new PIXI.Container();
		let containerPopup: PIXI.Container = new PIXI.Container();

		Cool.GameCanvas.stage.addChild(containerMain);
		Cool.GameCanvas.stage.addChild(containerUI);
		Cool.GameCanvas.stage.addChild(containerPopup);

		Cool.LayerManager.addLayer(Layers.MAIN, containerMain);
		Cool.LayerManager.addLayer(Layers.POPUP, containerPopup);
		Cool.LayerManager.addLayer(Layers.UI, containerUI);

		let map: ExamplePathMap = new ExamplePathMap();
		Cool.ViewManager.initialize(map);
	}

	private showGame(): void
	{
		Cool.ViewManager.goTo(Paths.SPLASH, {});
		// Cool.ViewManager.goTo(Paths.GAME, {});
	}

	private update(delta: number): void
	{
		// Update ticker
		Groups.Updateables.update(delta, PIXI.ticker.shared.elapsedMS);
	}
}

let main: Main = new Main();
main.initGameLoop();
