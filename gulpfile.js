// importing modules

//NPM modules
let gulp = require("gulp");
let clean = require("gulp-clean");
let gulptslint = require("gulp-tslint");
let tslint = require("tslint");
let fs = require("fs");
let webpack = require("webpack");
let jsonminify = require('jsonminify');
let path = require('path');
let audiosprite = require('gulp-audiosprite');
let fbdeploy = require('fbinstant-deploy');
let WebpackDevServer = require("webpack-dev-server");

const {
	hashElement
} = require('folder-hash');

//helper scripts
let packageJSON = require('./package.json');
let buildTargets = require('./build_scripts_and_config/buildTargets');
let webpackConfig = require('./build_scripts_and_config/webpack.config');
let AssetMapGenerator = require("./build_scripts_and_config/AssetmapGeneration");
let ImageAtlas = require('./build_scripts_and_config/ImageAtlas');
let SpineAssets = require('./build_scripts_and_config/SpineAssets')

// project config
let PROJECT_CONFIG = require("./build_scripts_and_config/project.config");

//Passed arguments
var argv = require('yargs').argv;

/**
 * Google Sheet To JSON
 * https://www.npmjs.com/package/spreadsheet-to-json
 */
const {
	extractSheets
} = require("spreadsheet-to-json");

/**
 * Contains all the info needed by the game to load the assets.
 * Is saved to a jason as part of the build process
 */
let assetManifest = {
	atlases: [],
	sounds: [],
	fonts: [],
	spines: [],
	qualitySettings: [],
	designDimensions: {}
};

let supportedLanguages = [];

let buildTarget;
let deploySettings;
let useMocks;

/**
 * Check if assets need to be rebuild
 */
let shouldBuildAssets = true;

gulp.task("build-localization", function (done) {

	if (!PROJECT_CONFIG.GOOGLESHEET_CONFIG.SPREADSHEET_KEY && !PROJECT_CONFIG.GOOGLESHEET_CONFIG.CREDENTIALS_PATH) {
		done();
		return;
	}

	extractSheets({
			spreadsheetKey: PROJECT_CONFIG.GOOGLESHEET_CONFIG.SPREADSHEET_KEY,
			credentials: PROJECT_CONFIG.GOOGLESHEET_CONFIG.CREDENTIALS_PATH,
			sheetsToExtract: PROJECT_CONFIG.GOOGLESHEET_CONFIG.SHEETS_TO_EXTRACT,
		},
		function (err, data) {

			if (err) {
				throw new Error("Error encountered while getting localization sheet!", err);
			}

			if (data) {

				for (const sheet in data) {
					if (data.hasOwnProperty(sheet)) {
						let objectToSaveAsJSON = {};
						const localizationEntries = data[sheet];
						localizationEntries.forEach(entry => {

							let objectToAddKeysTo;

							for (const key in entry) {
								if (entry.hasOwnProperty(key)) {
									if (key === "id-id") {
										objectToAddKeysTo = objectToSaveAsJSON[entry[key]] = {};
										continue;
									}

									if (!supportedLanguages.includes(key)) {
										supportedLanguages.push(key);
									}

									objectToAddKeysTo[key] = entry[key];
								}
							}
						});

						require('fs').writeFileSync(PROJECT_CONFIG.PATHS.OUTPUT_PATH_LOCALIZATION + PROJECT_CONFIG.PATHS.OUTPUT_LOCALIZATION_FILENAME, JSON.stringify(objectToSaveAsJSON));
					}
				}

				done();
			} else {
				throw new Error("No data found in sheet.");
			}
		}
	);
});

/**
 * Checks the source code for Programmatic or Stylistic errors
 */
gulp.task("lint", () => {
	let config = {
		formatter: "verbose",
		emitError: (process.env.CI) ? true : false,
		program: tslint.Linter.createProgram(PROJECT_CONFIG.PATHS.TSCONFIG)
	};
	return gulp.src([
			PROJECT_CONFIG.PATHS.SRC + "**/**.ts",
		])
		.pipe(gulptslint(config))
		.pipe(gulptslint.report());
});

/**
 * Cleans up the (build) folder
 */
gulp.task("clean", (done) => {

	if (!shouldBuildAssets) {
		done();
		return;
	}

	return gulp.src('build', {
			read: false,
			allowEmpty: true
		})
		.pipe(clean())
		.on("end", done);
});

gulp.task("start-dev-server", (callback) => {
	// Start a webpack-dev-server
	var webpackConfiguration = webpackConfig({
		target: buildTarget,
		useMock: useMocks
	});
	startWebpackDevServer(webpackConfiguration, callback);
});

function startWebpackDevServer(config, callback) {
	var webpackConfiguration = webpack(config);

	new WebpackDevServer(webpackConfiguration,
		config.devServer
	).listen(8080, "localhost", function (err) {
		if (err) throw new gutil.PluginError("webpack-dev-server", err);

		// keep the server alive or continue?
		// callback();
	});
}

/**
 * Builds the bundle.js file + index.html
 */
gulp.task("build-code", function (done) {

	var webpackConfiguration = webpackConfig({
		target: buildTarget,
		useMock: useMocks
	});

	return webpack(
		webpackConfiguration,
		function (err, stats) {

			var errors = stats.compilation.errors;
			for (var i = 0; i < errors.length; i++) {
				console.error(errors[i].message);
			}

			if (err) {
				console.error(err);
			}
			done();
		}
	);
});

/**
 * Create Hash File to check asset folder changes
 */

gulp.task('build-hash', function (done) {
	let hashFile,
		previousHash;

	try {
		hashFile = fs.readFileSync(PROJECT_CONFIG.PATHS.BUILD_SCRIPTS_AND_CONFIG + "Hash.json");

		previousHash = JSON.parse(hashFile);

		compareHash(done, previousHash.hash);
	} catch (err) {
		if (err.code === 'ENOENT') {
			compareHash(done);
		} else {
			throw err;
		}
	}
});

function compareHash(done, previousHash) {
	hashElement('assets', PROJECT_CONFIG.PATHS)
		.then(hash => {
			if (previousHash === hash.hash) {
				if (
					fs.existsSync(PROJECT_CONFIG.PATHS.OUTPUT_PATH_ATLASES) &&
					fs.existsSync(PROJECT_CONFIG.PATHS.OUTPUT_PATH_SPINE) &&
					fs.existsSync(PROJECT_CONFIG.PATHS.OUTPUT_PATH_AUDIO) &&
					fs.existsSync(PROJECT_CONFIG.PATHS.OUTPUT_PATH_FONT)
				) {
					shouldBuildAssets = false;
				}
			}
			fs.writeFileSync(PROJECT_CONFIG.PATHS.BUILD_SCRIPTS_AND_CONFIG + "Hash.json", JSON.stringify({
				"hash": hash.hash
			}));
			done();
		})
		.catch(error => {
			return console.error('hashing failed:', error);
		});
}

/**
 * Sub-task for writing the asset manifest json
 */
gulp.task('build-assets-manifest', function (done) {

	fs.writeFileSync(PROJECT_CONFIG.PATHS.OUTPUT_PATH_MANIFEST, jsonminify(JSON.stringify(assetManifest, null, 4)));
	done();
});

/**
 * Create asset declaration file
 */
gulp.task('generate-assetmap', function (done) {

	let fileContent = AssetMapGenerator.generateFromManifest(assetManifest, supportedLanguages);

	// assetManifest
	require('fs').writeFileSync(PROJECT_CONFIG.PATHS.ASSETMAP_OUTPUT + PROJECT_CONFIG.ASSETMAP_CONFIG.FILENAME, fileContent);
	done();
});

/**
 * Sub-task for adding fonts to the build
 */

gulp.task('add-fonts', (done) => {

	let files = fs.readdirSync(PROJECT_CONFIG.PATHS.FONT_SRC_PATH);

	for (let i = 0; i < files.length; i++) {
		if (files[i].match(/\.(ttf|eot|woff|woff2)$/)) {
			assetManifest.fonts.push({
				alias: files[i].replace(/\.[^/.]+$/, ""),
				src: [(PROJECT_CONFIG.PATHS.RELATIVE_OUTPUT_FONT + files[i]).replace(/\\/g, '/')]
			});

		} else {
			throw Error("Wrong file type included see at: " + files[i]);
		}
	}

	gulp.src(PROJECT_CONFIG.PATHS.FONT_SRC_PATH + '*')
		.pipe(gulp.dest(PROJECT_CONFIG.PATHS.OUTPUT_PATH_FONT))
		.on('end', done);
});

/**
 * Sub-task for building the atlases
 */
gulp.task('build-assets-images', async function (done) {

	// define folder
	let folders = fs.readdirSync(PROJECT_CONFIG.PATHS.IMAGE_SRC_PATH)
		.filter(function (file) {
			return fs.statSync(path.join(PROJECT_CONFIG.PATHS.IMAGE_SRC_PATH, file)).isDirectory();
		});

	// add 'folderless' images folder (default)
	folders.push('*');

	// loop through image folders
	for (let i = 0; i < folders.length; i++) {

		await ImageAtlas.buildFromFolder(folders[i], assetManifest);
	}
	done();
});


gulp.task('build-audiosprites', async (done) => {

	// define folder
	let folders = fs.readdirSync(PROJECT_CONFIG.PATHS.AUDIO_SRC_PATH)
		.filter(function (file) {
			return fs.statSync(path.join(PROJECT_CONFIG.PATHS.AUDIO_SRC_PATH, file)).isDirectory();
		});

	// add 'folderless' images folder (default)
	folders.push('');

	// loop through main folders
	for (let i = 0; i < folders.length; i++) {
		await makeAudioSprite(folders[i]);
	}

	done();
});

function makeAudioSprite(folderName) {

	let spriteName = folderName;

	if (folderName == '') {
		spriteName = 'default';
	}

	assetManifest.sounds.push({
		alias: spriteName,
		src: [(PROJECT_CONFIG.PATHS.RELATIVE_OUTPUT_AUDIO + spriteName + ".json").replace(/\\/g, '/')]
	});

	return new Promise((resolve, reject) => {
		gulp.src(PROJECT_CONFIG.PATHS.AUDIO_SRC_PATH + folderName + "/**.{wav,mp3,ogg,raw}")
			.pipe(audiosprite({
				format: 'jukebox',
				output: spriteName,
				channels: 1,
				bitrate: 64,
				export: "ogg,mp3",
				log: "notice"
			}))
			.pipe(gulp.dest(PROJECT_CONFIG.PATHS.OUTPUT_PATH_AUDIO))
			.on('end', resolve);
	});
}

gulp.task('build-spine-files', async (done) => {

	await SpineAssets.build(assetManifest);
	done();
});

/**
 * Add fbapp config file.
 */
gulp.task('add-fbapp-config', function () {
	return gulp.src(PROJECT_CONFIG.PATHS.FBAPP_CONFIG)
		.pipe(gulp.dest(PROJECT_CONFIG.PATHS.OUTPUT_PATH));
});

/**
 * Deploys game to facebook
 */
gulp.task('deploy-to-facebook', function (done) {


	fbdeploy.deploy(PROJECT_CONFIG.PATHS.OUTPUT_PATH, deploySettings.access_token, deploySettings.appId, packageJSON.version)
		.then(() => {
			console.log("Deployment successful!");
			done();

			gulp.src(__dirname + '\\build.zip', {
					read: false
				})
				.pipe(clean());
		})
		.catch((err) => {
			console.log("Error", err);
		});
});

/**
 * Buils all the game assets
 */
gulp.task("build-assets", function (done) {

	if (!shouldBuildAssets) {
		done();
		return;
	}

	fs.mkdirSync(PROJECT_CONFIG.PATHS.OUTPUT_PATH);

	//create the folder structure for the assets
	fs.mkdirSync(PROJECT_CONFIG.PATHS.OUTPUT_PATH_AUDIO);
	fs.mkdirSync(PROJECT_CONFIG.PATHS.OUTPUT_PATH_ATLASES);
	fs.mkdirSync(PROJECT_CONFIG.PATHS.OUTPUT_PATH_FONT);
	fs.mkdirSync(PROJECT_CONFIG.PATHS.OUTPUT_PATH_SPINE);

	gulp.series('build-assets-images', 'build-audiosprites', 'build-spine-files', 'add-fonts', 'build-localization', 'generate-assetmap', 'build-assets-manifest')(done);
});

gulp.task("set-environment", function (done) {

	if (argv.production !== undefined) {
		buildTarget = buildTargets.PROD;
		deploySettings = PROJECT_CONFIG.DEPLOY_CONFIG.PROD
		useMocks = false;
	}
	if (argv.staging !== undefined) {
		buildTarget = buildTargets.STAGE;
		deploySettings = PROJECT_CONFIG.DEPLOY_CONFIG.STAGE
		useMocks = false;
	}

	//DEFAULT = DEV
	if (buildTarget === undefined) {
		buildTarget = buildTargets.DEV;
		deploySettings = PROJECT_CONFIG.DEPLOY_CONFIG.DEV;
	}

	if (argv.usemocks !== undefined) {
		useMocks = true;
	} else {
		useMocks = false;
	}

	done();
});


gulp.task('build', gulp.series('build-hash', 'set-environment', 'clean', 'build-assets'));

gulp.task('deploy', gulp.series('build', 'build-code'));

gulp.task("webpack-dev-server", gulp.series('build', 'start-dev-server'));